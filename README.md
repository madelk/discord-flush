Discord flusher will delete messages 25 at a time from a given channel.

You'll need to grab your discord token, search for rooms and enter that room ID to be flushed, but you can check what will be deleted before you clear the messages

Nothing you enter is persisted in any way