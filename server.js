var http = require('http');
var path = require('path');

var express = require('express');

var router = express();
var server = http.createServer(router);

router.use(express.static(path.resolve(__dirname, 'client')));
// https://discord.js.org/#/docs/main/stable/class/TextChannel?scrollTo=fetchMessages
server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Server listening at", addr.address + ":" + addr.port);
});

const Discord = require("discord.js");

router.get('/getRooms', function (req, res) {
  const client = new Discord.Client();
  client.login(req.query.token)
    .catch((err)=> {
      console.log('some error' + err);
      res.json({error: err.message});
    });
  client.on('ready', () => {
    console.log(client.channels)
    res.json({channels: client.channels.array()});
  });
  // client.disconnect();
})

function GetMessages(req, res, callback) {
    const client = new Discord.Client();
  const token = req.query.token;
  const userName = req.query.userName;
  const roomid = req.query.roomid;
  client.login(req.query.token)
    .catch((err)=> {
      console.log('some error' + err);
      res.json({error: err.message});
    });
  client.on('ready', () => {
    var channel = client.channels.get(roomid);
    if (!channel) {
      console.log('Channel not found');
      res.json('Channel not found');
      return;
    }
    var user = client.users.find("username", userName);
    if (!user) {
      console.log('User not found');
      res.json('User not found');
      return;
    }
    channel.search({
      author: user.id
    }).then(searchRes => {
      var allMessages = [];
      for (var i = 0; i < searchRes.messages.length; i++) {
        var message = searchRes.messages[i].find(m => m.hit);
        allMessages.push(message);
      }
      callback(allMessages);
      // res.json(messageText);
    }).catch((err)=> {
      console.log('some error' + err);
      res.json({error: err.message});
    })
  });
}

router.get('/clearMessages', function (req, res) {
  GetMessages(req, res, (messages) => {
    for (var i = 0; i < messages.length; i++) {
      messages[i].delete();
    }
    res.json("I deleted " + messages.length + " messages");
  });
});

router.get('/testMessages', function (req, res) {
  GetMessages(req, res, (messages) => {
    var messageText = [];
    for (var i = 0; i < messages.length; i++) {
      messageText.push(messages[i].content);
    }
    res.json({messages: messageText});
  });
});