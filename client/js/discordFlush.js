var _output = $('#output');

$('#getRooms').click(function() {
  var dataToPost = {
      'token': $('#token').val()
    };
    $.ajax({
      dataType: "json",
      url: '/getRooms',
      data: dataToPost,
      success: (result) => {
        if (result.error) {
          _output.text(result);
          return
        }
        if (result.channels) {
          BuildChannels(result.channels)
        }
      }
    })
});

$('#clearMessages').click(function() {
  var dataToPost = {
      'token': $('#token').val(),
      'userName': $('#userName').val(),
      'roomid': $('#roomid').val()
    };
    $.ajax({
      dataType: "json",
      url: '/clearMessages',
      data: dataToPost,
      success: (result) => {
        _output.text(result);
      }
    })
});

$('#testMessages').click(function() {
  var dataToPost = {
      'token': $('#token').val(),
      'userName': $('#userName').val(),
      'roomid': $('#roomid').val()
    };
    $.ajax({
      dataType: "json",
      url: '/testMessages',
      data: dataToPost,
      success: (result) => {
        if (!result.messages) {
          _output.text(result);
          return;
        }
        var textToShow = "";
        for (var i = 0; i < result.messages.length; i++) {
          textToShow = textToShow + '<p>'+ result.messages[i] +'</p>';
        }
        _output.html(textToShow);
      }
    })
});

function BuildChannels(channels) {
  var message = [];
  for (var i = 0; i < channels.length; i++) {
    var channel = channels[i];
    switch(channel.type) {
      case "dm":
        message.push({id: channel.id, name: channel.recipient.username});
        break;
      case "text":
        message.push({id: channel.id, name: channel.guild.name+' '+channel.name});
        break;
    }
  }
  var table = $('<table class="table">')
  table.append('<tr><th>id</th><th>name</th></tr>')
  for (var i = 0; i < message.length; i++) {
    table.append('<tr><td>' + message[i].id + '</td><td>'+ message[i].name +'</td></tr>')
  }
  _output.text('');
  _output.append(table);
}
 